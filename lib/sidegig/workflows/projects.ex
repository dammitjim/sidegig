defmodule Sidegig.Workflows.Project do
  use Ecto.Schema
  import Ecto.Changeset

  alias Sidegig.Accounts.User

  schema "projects" do
    field :description, :string
    field :name, :string
    field :slug, :string

    belongs_to :owner, User

    timestamps()
  end

  @doc false
  def changeset(projects, attrs) do
    projects
    |> cast(attrs, [:name, :description])
    |> validate_required([:name, :description])
    |> slugify()
  end

  defp slugify(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{name: name}} ->
        put_change(changeset, :slug, Slugger.slugify_downcase(name))
      _ ->
        changeset
    end
  end
end
