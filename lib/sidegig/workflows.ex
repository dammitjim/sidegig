defmodule Sidegig.Workflows do
  @moduledoc """
  The Workflows context.
  """

  import Ecto.Query, warn: false
  alias Sidegig.Repo

  alias Sidegig.Workflows.Project

  @doc """
  Returns the list of projects.

  ## Examples

      iex> list_projects()
      [%Project{}, ...]

  """
  def list_projects do
    Repo.all(Projects)
  end

  @doc """
  Gets a single projects.

  Raises `Ecto.NoResultsError` if the Projects does not exist.

  ## Examples

      iex> get_projects!(123)
      %Project{}

      iex> get_projects!(456)
      ** (Ecto.NoResultsError)

  """
  def get_projects!(id), do: Repo.get!(Projects, id)

  @doc """
  Creates a projects.

  ## Examples

      iex> create_projects(%{field: value})
      {:ok, %Project{}}

      iex> create_projects(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_projects(attrs \\ %{}) do
    %Project{}
    |> Project.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a projects.

  ## Examples

      iex> update_projects(projects, %{field: new_value})
      {:ok, %Project{}}

      iex> update_projects(projects, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_projects(%Project{} = projects, attrs) do
    projects
    |> Project.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Projects.

  ## Examples

      iex> delete_projects(projects)
      {:ok, %Project{}}

      iex> delete_projects(projects)
      {:error, %Ecto.Changeset{}}

  """
  def delete_project(%Project{} = project) do
    Repo.delete(project)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking projects changes.

  ## Examples

      iex> change_projects(projects)
      %Ecto.Changeset{source: %Project{}}

  """
  def change_projects(%Project{} = project) do
    Project.changeset(project, %{})
  end

  def list_projects_for(user) do
    from(p in Project, where: p.owner_id == ^user.id)
    |> Repo.all()
  end

  def has_access_to_project?(user, project) do
  end

  def get_project_by_slug(slug) do
  end
end
