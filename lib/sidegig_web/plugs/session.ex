defmodule SidegigWeb.Plugs.Session do
  import Plug.Conn

  alias Sidegig.Accounts

  def init(opts), do: opts

  def call(conn, _opts) do
    user_id = get_session(conn, :user_id)
    user = user_id && Accounts.get_user(user_id)
    assign(conn, :user, user)
  end
end
