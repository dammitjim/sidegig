defmodule SidegigWeb.Plugs.AnonRequired do
  import Plug.Conn
  import Phoenix.Controller

  alias SidegigWeb.Router.Helpers, as: Routes

  def init(opts), do: opts

  def call(conn, _opts) do
    if conn.assigns.user do
      conn
      |> put_flash(:error, "You must be logged out to access that page")
      |> redirect(to: Routes.page_path(conn, :index))
      |> halt()
    else
      conn
    end
  end
end
