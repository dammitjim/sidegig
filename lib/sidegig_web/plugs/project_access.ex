defmodule SidegigWeb.Plugs.ProjectAccess do
  import Plug.Conn
  import Phoenix.Controller

  alias Sidegig.Workflows
  alias SidegigWeb.Router.Helpers, as: Routes

  def init(opts), do: opts

  def call(conn, opts) do
    project = Workflows.get_project_by_slug(conn.params[opts.key])

    if Workflows.has_access_to_project?(conn.assigns.user, project) do
      conn
      |> assign(:project, project)
    else
      conn
      |> put_flash(:error, "You do not have access to this project")
      |> redirect(to: Routes.page_path(conn, :index))
      |> halt()
    end
  end
end
