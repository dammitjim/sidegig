defmodule SidegigWeb.SessionController do
  use SidegigWeb, :controller

  alias SidegigWeb.Auth

  plug SidegigWeb.Plugs.AnonRequired when action in [:new, :create]

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{"credentials" => %{"email" => email, "password" => password}}) do
    case Auth.login(conn, email, password) do
      {:ok, conn} ->
        conn
        |> put_flash(:info, "Welcome back")
        |> redirect(to: Routes.page_path(conn, :index))

      {:error, _reason, conn} ->
        conn
        |> put_flash(:error, "Invalid email / password combination")
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> Auth.logout()
    |> put_flash(:info, "You have been logged out.")
    |> redirect(to: Routes.page_path(conn, :index))
  end
end
