defmodule SidegigWeb.ProjectController do
  use SidegigWeb, :controller

  alias Sidegig.Workflows

  plug SidegigWeb.Plugs.ProjectAccess, %{key: "slug"} when action not in [:index]

  def index(conn, _params) do
    # load all projects that the user is a part of
    projects = Workflows.list_projects_for(conn.assigns.user)
    render(conn, "index.html", projects: projects)
  end

  def show(conn, _params) do
    # load an individual project provided the user is a part of it
    render(conn, "show.html", project: conn.assigns.project)
  end

  def new(conn, _params) do
  end

  def create(conn, %{"project" => %{"name" => name, "description" => description}}) do
  end
end
