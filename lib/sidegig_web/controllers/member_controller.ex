defmodule SidegigWeb.MemberController do
  use SidegigWeb, :controller

  plug SidegigWeb.Plugs.ProjectAccess, except: [:index], key: "project_slug"

  def index(conn, %{"project_slug" => project_slug}) do

  end
end
