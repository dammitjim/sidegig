defmodule SidegigWeb.AccountController do
  use SidegigWeb, :controller

  alias Sidegig.Accounts
  alias SidegigWeb.Auth

  plug SidegigWeb.Plugs.LoginRequired when action in [:index, :show, :edit, :update]
  plug SidegigWeb.Plugs.AnonRequired when action in [:new, :create]

  def show(conn, _params) do
    render(conn, "show.html")
  end

  def new(conn, _params) do
    changeset = Accounts.start_registration(%Accounts.User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"account" => account_params}) do
    case Accounts.register_user(account_params) do
      {:ok, user} ->
        conn
        |> Auth.login(user)
        |> put_flash(:success, "Your account has been created and you have been logged in.")
        |> redirect(to: Routes.page_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, _params) do
  end

  def update(conn, _params) do
  end
end
