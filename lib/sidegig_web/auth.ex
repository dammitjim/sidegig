defmodule SidegigWeb.Auth do
  import Plug.Conn

  alias Sidegig.Accounts

  def login(conn, user) do
    start_user_session(conn, user)
  end

  def login(conn, email, pass) do
    case Accounts.authenticate(email, pass) do
      {:ok, user} -> {:ok, start_user_session(conn, user)}
      {:error, :unauthorized} -> {:error, :unauthorized, conn}
      {:error, :not_found} -> {:error, :not_found, conn}
    end
  end

  def logout(conn) do
    stop_user_session(conn)
  end

  defp start_user_session(conn, user) do
    conn
    |> assign(:current_user, user)
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end

  defp stop_user_session(conn) do
    configure_session(conn, drop: true)
  end
end
