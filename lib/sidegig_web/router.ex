defmodule SidegigWeb.Router do
  use SidegigWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug SidegigWeb.Plugs.Session
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SidegigWeb do
    pipe_through :browser

    get "/", PageController, :index

    get "/login", SessionController, :new
    post "/login", SessionController, :create
    get "/logout", SessionController, :delete

    get "/account", AccountController, :show
    get "/account/register", AccountController, :new
    post "/account/register", AccountController, :create
    get "/account/manage", AccountController, :edit
    put "/account/manage", AccountController, :update

    resources "/projects", ProjectController, param: "slug", only: [:index, :show, :new, :create] do
      resources "/members", MemberController
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", SidegigWeb do
  #   pipe_through :api
  # end
end
