defmodule Sidegig.Repo.Migrations.CreateProjects do
  use Ecto.Migration

  def change do
    create table(:projects) do
      add :name, :string
      add :slug, :string
      add :description, :text

      add :owner_id, references(:users)

      timestamps()
    end

    create unique_index(:projects, :slug)
  end
end
