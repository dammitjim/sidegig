defmodule Sidegig.WorkflowsTest do
  use Sidegig.DataCase

  alias Sidegig.Workflows

  describe "projects" do
    alias Sidegig.Workflows.Projects

    @valid_attrs %{description: "some description", name: "some name"}
    @update_attrs %{description: "some updated description", name: "some updated name"}
    @invalid_attrs %{description: nil, name: nil}

    def projects_fixture(attrs \\ %{}) do
      {:ok, projects} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Workflows.create_projects()

      projects
    end

    test "list_projects/0 returns all projects" do
      projects = projects_fixture()
      assert Workflows.list_projects() == [projects]
    end

    test "get_projects!/1 returns the projects with given id" do
      projects = projects_fixture()
      assert Workflows.get_projects!(projects.id) == projects
    end

    test "create_projects/1 with valid data creates a projects" do
      assert {:ok, %Projects{} = projects} = Workflows.create_projects(@valid_attrs)
      assert projects.description == "some description"
      assert projects.name == "some name"
    end

    test "create_projects/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Workflows.create_projects(@invalid_attrs)
    end

    test "update_projects/2 with valid data updates the projects" do
      projects = projects_fixture()
      assert {:ok, %Projects{} = projects} = Workflows.update_projects(projects, @update_attrs)
      assert projects.description == "some updated description"
      assert projects.name == "some updated name"
    end

    test "update_projects/2 with invalid data returns error changeset" do
      projects = projects_fixture()
      assert {:error, %Ecto.Changeset{}} = Workflows.update_projects(projects, @invalid_attrs)
      assert projects == Workflows.get_projects!(projects.id)
    end

    test "delete_projects/1 deletes the projects" do
      projects = projects_fixture()
      assert {:ok, %Projects{}} = Workflows.delete_projects(projects)
      assert_raise Ecto.NoResultsError, fn -> Workflows.get_projects!(projects.id) end
    end

    test "change_projects/1 returns a projects changeset" do
      projects = projects_fixture()
      assert %Ecto.Changeset{} = Workflows.change_projects(projects)
    end
  end
end
